# audio processing via MQTT

## Forword - WIP warning
This project is work in progress and is yet not ready for production purposes as APIs can suddenly change at the moment. 
Anyway, support for different languages are now given using the `LANGUAGE` environment variable in the `.env` file. Supported languages are the ones Silero supports at the time of writing: `de` (German), `en` (English) and `es` (Spanish).

**Important**
`wakeword2mqtt` currently does not run properly inside a container. The root cause is still unknown.
Therefore the service must be run _natively_ on the machine on its own.

## What is this?
This is a project to help with IoT projects handling the speech side for them - on premise / without any "cloud". It's intended to be working with one docker-compose file as a one-stop solution but could be split up into several machines in case of low cpu.
However, the service `wakeword2mqtt` needs access to the wan-network in order to check the `Porcupine` license (see below).

Conceptually, the audio2mqtt service continiously streams audio-packets from the microphone to a MQTT server. All audio data is encrypted with a customer set password. Another service - wakeword2mqtt - constantly listens to that audio and waits for a given wakeword to appear. On successful detection of the wakeword, the service pubishes a "detected"-event to the MQTT server.
The third - and last - service namely speech2mqtt listens to that detection-topic and begins to listen to the audio, too. Note that the speech2mqtt service only listens if the detection-event is received. There is no constant speech to text translation.

At the current time of implementation, the speech2mtt service stops listening to the audio if one of the folliwing cases become true:
- The volume of the audio drops for `n` seconds below a given threshold
- A timeout (max listening duration) is reached

until then, all audio is buffered and finally sent to the recognition API as a WAV audio. The recognition API is currently implemented as a very simple Python3 REST API and runs locally (or on a host of your choice). There, the audio is transposed to text and sent back as a response to the REST call. The transposed text is then sent back from speech2mqtt to the MQTT server for processing. The processing of that text is not part of this repository and should be implemented by you - because it is super individual what you expect.

You should subscribe to topic `speech2mqtt/transcription/{audioDeviceIndex}` to receive the recognized text. See topics section for details.

```
    Note: 
    AF = audio-frame
    EAF = encrypted audio frame
    EVNT = detection event

    [audio2mqtt] --> EAF --\  [wakeword2mqtt] \
        /|\                |      /|\         | 
         |                EAF      |        EVNT
         AF                |      EAF         |
         |                \|/      |          |
    [audio device]    [--- MQTT server ---] <-/
                           |     |     /|\
                          EAF  EVNT     |
                           |     |     text ---- here you go!
                          \|/   \|/     |
                     /-> [speech2mqtt] -/
                     |        |
                     |   WAV-recording
                   text       |
                     |       \|/
                     \---- speechapi]
```

## Topics
### audio2mqtt
publishes to topic `audio2mqtt/pcm/{AudioDeviceIndex}` (later on called encrypted audio topic) with encrypted PCM audio data as plain binary blob. As you can see, the AudioDeviceIndex theoretically enables the use of more than one audio device / audio2mqtt service.
### wakeword2mqtt
listens to encrypted audio topic and publishes to topic `wakeword2mqtt/detection/{AudioDeviceIndex}` (later on called wake topic) in JSON format:
```
{
    "timestamp": 123456789, // unix timestamp of detection
    "word": "computer" // detected wakeword
}
```
### speech2mqtt
listens to wake topic and publishes the transposed text to topic `speech2mqtt/transcription/{AudioDeviceIndex}` in JSON format:
```
{
    "text": "some detected text", // transposed text
    "took": 0.123 // time in seconds the detection needed
}
```

Additionally, the start, the stop and the current audio level are published. Start and Stop of recording are published on topic `speech2mqtt/{start|stop}/{AudioDeviceIndex}` with the JSON content
```
{
    "timestamp": 123456789 // unix timestamp of event
}
```

Level will be published with every audio frame to topic `speech2mqtt/level/{AudioDeviceIndex}` with the JSON content
```
{
    "timestamp": 123456789, // unix timestamp of event
    "level_percent": 80 // microphone audio volume percent
}
```
only while recording is active.

## Services

### audio2mqtt
Constantly records audio from a device and sends the audio packets as an encrypted stream to a MQTT server.

### wakeword2mqtt
Constantly listens to the audio stream and sends a detection event to the MQTT server if a given wakeword is recognized. [Picovoice Porcupine](https://picovoice.ai/docs/porcupine) is used here so you need to get n API key from them. To see what you need (free or paid service), begin [here](https://picovoice.ai/pricing). Free pricing includes up to 3 devices at time of writing and should be enough for use in this project. Anyway this project is not limited to Porcupine. If you know some other wakeword detection engines feel free to  implement and PR them as a new service that we all could alternatively use.

### speechapi
A simple Python3 REST API for speech recognition. Uses the aweseome [silero models](https://github.com/snakers4/silero-models) to transpose speech to text from a given WAV-input (which will be sent within the call as form-data). On successful recognition, the REST call returns the recognized speech as text. Unfortunately this service can not handle audio streams but only complete recordings. If you have a better speech to text engine, feel free implement and PR as a new service (so to replace the speech2mqtt service).

### speech2mqtt
Housekeeping for the speech recognition. As I was too lazy to implement the audio encrption in Python, I decided to have this service to forward recordings to the speechapi. Conceptually this service waits for a detection event from the MQTT and begins buffering audio until a maximum duration or volume drops below a given level for a given time.

## How to build
### Multi-Architecture
`env $(cat .env | grep -v "#" | xargs) docker buildx bake --push --set "*.platform=linux/amd64,linux/arm64"` to build for aarch64/arm64 and amd64. The pushed image will containt both architectures.
### Local platform
If you want to just build for your platform, you can simply use `docker-compose build`

## How to use

### Checkout
use `git clone https://gitlab.com/DirkFaust/speech.git && cd speech && git submodule update --init --recursive`

### Pulling the images
Depending on your `docker-compose` version you may just issue a `docker-compose pull .` (old versions ignore that there is a `build` section) or you need to pull each image individually by using `cat docker-compose.yml | grep "image: " | cut -d ":" -f2 | xargs -I {} docker pull {}`.

### Running
Build or pull the images and use `docker-compose up -d .`.
It is **highly encouraged** that you change the `ENCRYPTION_PASSWORD` in the `.env` file.

Then, process the text on your own in a custom maybe text2intent engine. Remember to subscribe to the `speech2mqtt/transcription/{AudioDeviceIndex}` topic to receive text.

## Hint for Raspberry Pi + [ReSpeaker](https://wiki.seeedstudio.com/ReSpeaker_4_Mic_Array_for_Raspberry_Pi/) users

### Pi additional dependencies
`Portaudio` is required for `audio2mqtt` to work properly.
Install using `sudo apt-get install libportaudio2` on the host with the `audio2mqtt` service.

#### Test-run
via `docker run --rm -u 0 -e AUDIO_DEVICE_INDEX=1 -e MQTT_HOST=192.168.1.104 -e MQTT_PORT=1883 -e ENCRYPTION_PASSWORD=yourpassword --device /dev/snd:/dev/snd -v /usr/share/alsa:/usr/share/alsa -it registry.gitlab.com/dirkfaust/audio2mqtt:latest`

### [ReSpeaker](https://wiki.seeedstudio.com/ReSpeaker_4_Mic_Array_for_Raspberry_Pi/)
Latest aarch64 Raspi-OS is incompatible with the latest drivers from Seeed. I used [this fork](https://github.com/HinTak/seeed-voicecard.git) to get the stuff compiled ansh? d worky (note that there is just a `install.sh` and in difference to the original drivers no seperate `install_arm64`). Dangerously I just it just as it is and did not check for security issues. So no guarantees given.
As a side note, the audio device index for ReSpeaker on the Pi is **1**.
