FROM golang:1.18.2-bullseye as builder

RUN dpkg --add-architecture arm64 \
    && apt-get update \
    && apt-get install -y \
        gcc-aarch64-linux-gnu \
        libc6-dev-arm64-cross \
        pkg-config \
        portaudio19-dev \
        libportaudio2 \
        libportaudio2:arm64

# go main source file to use as entrypoint
ARG MAIN
ARG BINARYNAME
ARG SRC_DIR

ARG TARGETOS
ARG TARGETARCH

ENV GOOS="${TARGETOS}"
ENV GOARCH="${TARGETARCH}"

ENV MAIN="${MAIN}"
ENV BINARYNAME="${BINARYNAME}"
ENV SRC_DIR="${SRC_DIR}"

ENV CGO_ENABLED=1
ENV if [${TARGETARCH} = "arm64"]; then CC=${CC} ; else CC=aarch64-linux-gnu-gcc

COPY . /build

RUN cd /build/${SRC_DIR} && \
    go get ./... && \
    go build -o "${BINARYNAME}" -ldflags "-s -w" ${MAIN} && \
    /build/build/artifacts.sh "/build/${SRC_DIR}/${BINARYNAME}"

# ----- main image ----

FROM scratch

ARG SRC_DIR

ARG BINARYNAME
ENV BINARYNAME="${BINARYNAME}"

WORKDIR /
# Our own dependencies
COPY --from=builder /artifacts /
COPY --from=builder /usr/share/zoneinfo/${TZ} /usr/share/zoneinfo/${TZ}

WORKDIR /application
COPY --from=builder /build/${SRC_DIR}/${BINARYNAME} ./source

ENV MQTT_HOST="192.168.1.104"

USER 1001

ENTRYPOINT [ "./source" ] 
